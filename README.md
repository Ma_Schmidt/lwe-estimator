# Estimator for the Bit Security of LWE Instances with a Given Number of Samples

This repository is a fork of [LWE-Estimator](https://bitbucket.org/malb/lwe-estimator) to implemented required changes to take the number of samples into account. Like the original work, this [Sage](http://sagemath.org) module provides functions for estimating the bit security of [Learning with Errors](https://en.wikipedia.org/wiki/Learning_with_errors) instances.

For usage examples, description of content and contributions, please see the main project [LWE-Estimator](https://bitbucket.org/malb/lwe-estimator).

**!!! The changes of this repository are merged into the main project now. To make sure you get the latest version of the LWE-Estimator, see the main project [LWE-Estimator](https://bitbucket.org/malb/lwe-estimator) !!!**

## Citing ##

If you want to reference the changes to the LWE-Estimator regarding the number of samples, please cite

> Markus Schmidt and Nina Bindel.  
> *Estimation of the Hardness of the Learning with Errors Problem with a Restricted Number of Samples*.  
> Cryptology ePrint Archive, Report 2017/140, 2017.
> http://eprint.iacr.org/2017/140.